import { TestBed, inject } from '@angular/core/testing';

import { CuppaOAuthService } from './cuppa-oauth.service';

describe('CuppaOAuthService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CuppaOAuthService]
    });
  });

  it('should ...', inject([CuppaOAuthService], (service: CuppaOAuthService) => {
    expect(service).toBeTruthy();
  }));
});
