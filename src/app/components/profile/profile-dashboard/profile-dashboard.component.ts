import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { PreloadSelectedModules } from '../../../selective-preload-strategy';
import { ProfileService } from '../../../services/profile.service'
import { User } from '../profile.interface';
import { CuppaOAuthService } from '../../../services/cuppa-oauth.service';
import 'rxjs/add/operator/map';


@Component({
  selector: 'app-profile-dashboard',
  templateUrl: './profile-dashboard.component.html',
  styleUrls: ['./profile-dashboard.component.css']
})
export class ProfileDashboardComponent implements OnInit {
  private user: User = new User();

  constructor(
    private profileService: ProfileService,
    private authService: CuppaOAuthService
  ) { }

  ngOnInit() {
    this.getUserProfile();
  }

  getUserProfile() {

    this.profileService.getProfile().subscribe(
      profile => {
        console.log(profile);
        this.user = new User(profile._id, profile.displayName, profile.email, profile.picture, profile.provider, profile.provider_id)
        //this.
      },
      err => {
        console.log(err);
      });
  }
  logout() {
    this.authService.logout();
  }

}
