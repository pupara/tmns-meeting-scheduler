import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CuppaOAuthComponent } from './cuppa-oauth.component';

describe('CuppaOAuthComponent', () => {
  let component: CuppaOAuthComponent;
  let fixture: ComponentFixture<CuppaOAuthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CuppaOAuthComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CuppaOAuthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
