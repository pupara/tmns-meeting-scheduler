import { Component, OnInit, Input } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { CuppaOAuthService } from '../../services/cuppa-oauth.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-cuppa-oauth',
  templateUrl: './cuppa-oauth.component.html',
  styleUrls: ['./cuppa-oauth.component.css']
})
export class CuppaOAuthComponent implements OnInit {

  @Input()
  authConfig: any;

  constructor(public authService: CuppaOAuthService) { }

  ngOnInit() {
  }

  linkedinLogin() {
    this.authService.auth('linkedin', this.authConfig);
  }
  facebookLogin(){
    this.authService.auth('facebook', this.authConfig);
  }
  googleLogin(){
    this.authService.auth('google', this.authConfig);
  }

}
