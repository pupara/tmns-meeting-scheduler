import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  private authServerBaseUrl = 'http://localhost:3000';

  constructor() { }

  ngOnInit() {
  }

  private config = {
    "loginRoute": "login",
    "linkedin": {
      "authEndpoint": this.authServerBaseUrl + "/auth/linkedin",
      "clientId": "8176r44lz2ewos",
      "redirectURI": this.authServerBaseUrl + "/profile"
    },
    "facebook": {
      "authEndpoint": this.authServerBaseUrl + "/auth/facebook",
      "clientId": "314027552363970",
      "redirectURI": this.authServerBaseUrl + "/profile"
    },
    "google": {
      "authEndpoint": this.authServerBaseUrl + "/auth/google",
      "clientId": "768820273579-2jqt15s095qk895m1li5oolvscmant0v.apps.googleusercontent.com",
      "redirectURI": this.authServerBaseUrl + "/profile"
    }
  };

}
