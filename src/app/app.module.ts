import { NgModule } from '@angular/core';
import { CommonModule }   from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule, XHRBackend, RequestOptions } from '@angular/http';
import { RouterModule, Routes, CanLoad } from '@angular/router';
import { PreloadSelectedModules } from './selective-preload-strategy';

//Components
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { ProfileComponent } from './components/profile/profile.component';
import { ProfileDashboardComponent } from './components/profile/profile-dashboard/profile-dashboard.component';

//Modules
import { CuppaOAuthModule } from './components/cuppa-oauth/cuppa-oauth.module';

//Services
import { CuppaOAuthService } from './services/cuppa-oauth.service';
import { ProfileService } from './services/profile.service';

//Routes
// const loginRoutes: Routes = [
//   { path: '', component: LoginComponent },
//   { path: 'login', component: LoginComponent }
// ];

const appRoutes: Routes = [
  {
    path: '',
    component: LoginComponent },
  {
    path: 'login',
    component: LoginComponent },
  {
    path: 'profile',
    component: ProfileComponent,
    canActivate: [CuppaOAuthService],
    children: [
      {
        path: '',
        canActivateChild: [CuppaOAuthService],
        children: [
          {
            path: '',
            component: ProfileDashboardComponent
          }
        ]
      }
    ]
  }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ProfileComponent,
    ProfileDashboardComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    CuppaOAuthModule,
    CommonModule,
    //RouterModule.forChild(loginRoutes),
    RouterModule.forRoot(appRoutes)
  ],
  providers: [CuppaOAuthService, ProfileService, PreloadSelectedModules],
  bootstrap: [AppComponent]
})
export class AppModule { }
