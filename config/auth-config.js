module.exports = {
  // App Settings
  MONGO_URI: process.env.MONGO_URI || 'mongodb://localhost:27017/meetingscheduler',
  //MONGO_URI: process.env.MONGO_URI || 'mongodb://username:password@ds111489.mlab.com:11489/cuppaoauth',
  TOKEN_SECRET: process.env.TOKEN_SECRET || 'YOUR_UNIQUE_JWT_TOKEN_SECRET',

  // OAuth 2.0
  FACEBOOK_SECRET: process.env.FACEBOOK_SECRET || 'd80db67ea049b4e68b4442e009c13727',
  GOOGLE_SECRET: process.env.GOOGLE_SECRET || 'n-Wr7cV80SS5QYkoSct13I21',
  LINKEDIN_SECRET: process.env.LINKEDIN_SECRET || 'xxxxxx',
};
