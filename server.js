const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
const config = require('./config/auth-config');

// Connect to Database
mongoose.connect(config.MONGO_URI);

// On Connection
mongoose.connection.on('connected', () => {
  console.log('Connected to database ', config.MONGO_URI);
});

// On Error
mongoose.connection.on('error', (err) => {
  console.log('Database error ', err);
});

const app = express();
const users = require('./routes/users');
const port = 3000;

app.set('port', port);
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'public')));
app.use('/', users);

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'public/index.html'));
});

app.listen(port, function() {
  console.log('Express server listening on port ' + port);
});
