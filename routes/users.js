const express = require('express');
const router = express.Router();
const jwt = require('jwt-simple');
const moment = require('moment');
const config = require('../config/auth-config');
var request = require('request');

const User = require('../models/user');


function ensureAuthenticated(req, res, next) {
  if (!req.header('Authorization')) {
    return res.status(401).send({
      message: 'Please make sure your request has an Authorization header'
    });
  }
  var token = req.header('Authorization').split(' ')[1];

  var payload = null;
  try {
    payload = jwt.decode(token, config.TOKEN_SECRET);
  } catch (err) {
    return res.status(401).send({
      message: err.message
    });
  }

  if (payload.exp <= moment().unix()) {
    return res.status(401).send({
      message: 'Token has expired'
    });
  }
  req.user = payload.sub;
  next();
}

function createJWT(user) {
  var payload = {
    sub: user._id,
    iat: moment().unix(),
    exp: moment().add(14, 'days').unix()
  };
  return jwt.encode(payload, config.TOKEN_SECRET);
}

router.get('/api/profile', ensureAuthenticated, function(req, res) {
  User.findById(req.user, function(err, user) {
    console.log('user ', user);
    res.send(user);
  });
});

router.put('/api/me', ensureAuthenticated, function(req, res) {
  User.findById(req.user, function(err, user) {
    if (!user) {
      return res.status(400).send({
        message: 'User not found'
      });
    }
    user.displayName = req.body.displayName || user.displayName;
    user.email = req.body.email || user.email;
    user.save(function(err) {
      res.status(200).end();
    });
  });
});

// Google login

router.post('/auth/google', function(req, res) {
  var accessTokenUrl = 'https://www.googleapis.com/oauth2/v4/token';
  var peopleApiUrl = 'https://www.googleapis.com/oauth2/v2/userinfo?fields=email%2Cfamily_name%2Cgender%2Cgiven_name%2Chd%2Cid%2Clink%2Clocale%2Cname%2Cpicture%2Cverified_email';
  var params = {
    code: req.body.code,
    client_id: req.body.clientId,
    client_secret: config.GOOGLE_SECRET,
    redirect_uri: req.body.redirectUri,
    grant_type: 'authorization_code'
  };
  var token_request = 'code=' + req.body.code +
    '&client_id=' + req.body.clientId +
    '&client_secret=' + config.GOOGLE_SECRET +
    '&redirect_uri=' + req.body.redirectUri +
    '&grant_type=authorization_code';
  var request_length = token_request.length;
  // Step 1. Exchange authorization code for access token.
  request.post(accessTokenUrl, {
    body: token_request,
    headers: {
      'Content-type': 'application/x-www-form-urlencoded'
    }
  }, function(err, response, token) {
    var accessToken = JSON.parse(token).access_token;
    var headers = {
      Authorization: 'Bearer ' + accessToken
    };
    console.log('accessToken ', accessToken);

    // Step 2. Retrieve profile information about the current user.
    request.get({
      url: peopleApiUrl,
      headers: headers,
      json: true
    }, function(err, response, profile) {
      if (profile.error) {
        return res.status(500).send({
          message: profile.error.message
        });
      }

      User.findOne({
        email: profile.email
      }, function(err, existingUser) {
        if (existingUser && existingUser.provider == "google") {
          var token = createJWT(existingUser);
          res.send({
            token: token
          });
        } else if (existingUser && existingUser.provider != "google") {
          var user = {};
          user.provider_id = profile.id;
          user.provider = "google";
          user.email = profile.email;
          user.picture = profile.picture.replace('sz=50', 'sz=200');
          user.displayName = profile.name;
          User.findOneAndUpdate({
            email: existingUser.email
          }, user, function(err) {
            var token = createJWT(existingUser);
            res.send({
              token: token
            });
          });
        } else {
          var user = new User();
          user.provider_id = profile.id;
          user.provider = "google";
          user.email = profile.email;
          user.picture = profile.picture.replace('sz=50', 'sz=200');
          user.displayName = profile.name;
          user.save(function(err) {
            var token = createJWT(user);
            res.send({
              token: token
            });
          });
        }
        // var token = req.header('Authorization').split(' ')[1];
        // var payload = jwt.decode(token, config.TOKEN_SECRET);
      });
    });
  });
});

// Facebook login

router.post('/auth/facebook', function(req, res) {
  console.log('faceboooooooook');
  var fields = ['id', 'email', 'first_name', 'last_name', 'link', 'name', 'picture.type(large)'];
  var accessTokenUrl = 'https://graph.facebook.com/v2.5/oauth/access_token';
  var graphApiUrl = 'https://graph.facebook.com/v2.5/me?fields=' + fields.join(',');
  var params = {
    code: req.body.code,
    client_id: req.body.clientId,
    client_secret: config.FACEBOOK_SECRET,
    redirect_uri: req.body.redirectUri
  };

  // Step 1. Exchange authorization code for access token.
  request.get({
    url: accessTokenUrl,
    qs: params,
    json: true
  }, function(err, response, accessToken) {
    if (response.statusCode !== 200) {
      return res.status(500).send({
        message: accessToken.error.message
      });
    }

    // Step 2. Retrieve profile information about the current user.
    request.get({
      url: graphApiUrl,
      qs: accessToken,
      json: true
    }, function(err, response, profile) {
      if (response.statusCode !== 200) {
        return res.status(500).send({
          message: profile.error.message
        });
      }
      User.findOne({
        email: profile.email
      }, function(err, existingUser) {
        if (existingUser && existingUser.provider == "facebook") {
          var token = createJWT(existingUser);
          res.send({
            token: token
          });
        } else if (existingUser && existingUser.provider != "facebook") {
          var user = {};
          user.provider_id = profile.id;
          user.provider = "facebook";
          user.email = profile.email;
          user.picture = profile.picture.data.url;
          user.displayName = profile.name;
          User.findOneAndUpdate({
            email: existingUser.email
          }, user, function(err) {
            var token = createJWT(existingUser);
            res.send({
              token: token
            });
          });
        } else {
          var user = new User();
          user.provider_id = profile.id;
          user.provider = "facebook";
          user.email = profile.email;
          user.picture = profile.picture.data.url;
          user.displayName = profile.name;
          user.save(function(err) {
            var token = createJWT(user);
            res.send({
              token: token
            });
          });
        }
        // var token = req.header('Authorization').split(' ')[1];
        // var payload = jwt.decode(token, config.TOKEN_SECRET);
      });
    });
  });
});

module.exports = router;
