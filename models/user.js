const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/auth-config');

//user schema
const userSchema = mongoose.Schema({
  email: { type: String, lowercase: true},
  password: { type: String, select: false},
  displayName: String,
  picture: String,
  provider: String,
  provider_id: String,
  google: {
    provider: String,
    provider_id: String,
    token: String,
    displayName: String,
    picture: String
  },
  facebook: {
    provider: String,
    provider_id: String,
    token: String,
    displayName: String,
    picture: String
  },
});

const User = module.exports = mongoose.model('User', userSchema);

userSchema.pre('save', function(next) {
  var user = this;
  if (!user.isModified('password')) {
    return next();
  }
  bcrypt.genSalt(10, function(err, salt) {
    bcrypt.hash(user.password, salt, function(err, hash) {
      user.password = hash;
      next();
    });
  });
});

module.exports.comparePassword = function(candidatePassword, hash, callback) {
  bcrypt.compare(candidatePassword, hash, (err, isMatch) => {
    if(err) throw err;
    callback(null, isMatch);
  })
}

module.exports.getUserById = function(id, callback) {
  User.findById(id, callback);
}

module.exports.getUserByEmail = function(email, callback) {
  const query = {email: email};
  User.findOne(query, callback);
}

module.exports.addUser = function(newUser, callback) {
  bccrypt.genSalt(10, (err, salt) => {
    bccrypt.hash(newUser.password, salt, (err, hash) => {
      if(err) throw err;
      newUser.password = hash;
      newUser.save(callback);
    })
  })
}
